using Microsoft.EntityFrameworkCore;
namespace service1.Models
{
    public class Service1Context : DbContext
    {
        private Service1Context() { } //for .NET core
        public Service1Context(DbContextOptions<Service1Context> options) : base(options)
        {
        }
        public DbSet<LogItem> LogItems { get; set; }
    }
}