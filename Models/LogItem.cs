using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;
using System.Text.Json;
using System.ComponentModel.DataAnnotations;


namespace service1.Models
{
    public class LogItem
    {
        public long Id { get; set; }
        public long Priotity { get; set; }
        public DateTime Timestamp { get; set; }
        public string Message { get; set; }

        //https://www.npgsql.org/efcore/mapping/json.html?tabs=data-annotations%2Cjsondocument
        public JsonDocument Metadata { get; set; }

        //this is using hstore type in postgres
        public Dictionary<string, string> Tags { get; set; }

        //store string as json object.
        [ColumnAttribute(TypeName = "jsonb")]
        public string Json { get; set; }
    }
}