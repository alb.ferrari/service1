using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using service1.Models;

namespace service1.Controllers
{
    [ApiController]
    [Route("log")]
    public class LogController : ControllerBase
    {
        private readonly ILogger<LogController> _logger;
        private readonly LogReader _logReader;
        private readonly LogWriter _logWriter;

        public LogController(ILogger<LogController> logger, LogReader logReader, LogWriter logWriter)
        {
            _logger = logger;
            _logReader = logReader;
            _logWriter = logWriter;
        }

        [HttpGet]
        public IEnumerable<LogItem> Get()
        {
            return _logReader.GetAll();
        }

        [HttpGet("{id}", Name = "id")]
        public ActionResult<LogItem> GetById(long id)
        {
            var result = _logReader.GetById(id);
            if (result == null)
            {
                return NotFound();
            }
            return result;

        }
        public enum Method
        {
            sql, linq
        }
        [HttpGet("search")]
        public IEnumerable<LogItem> GetByPattern([FromQuery] string pattern, [FromQuery] Method method = Method.linq)
        {
            if (method == Method.sql)
            {//     //using LINQ
                return _logReader.GetByPatternSQL(pattern!);
            }
            return _logReader.GetByPatternLINQ(pattern!);
        }

        [HttpPost]
        public void Post(LogItem item)
        {
            item.Timestamp = DateTime.Now;
            _logWriter.Add(item);
        }
    }
}
