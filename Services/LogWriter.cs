using Microsoft.Extensions.Logging;
using service1.Models;

namespace service1
{
    public class LogWriter
    {
        private readonly ILogger<LogReader> _logger;
        private readonly Service1Context _context;

        public LogWriter(ILogger<LogReader> logger, Service1Context context)
        {
            _logger = logger;
            _context = context;
        }
        public void Add(LogItem item)
        {
            _context.Add(item!);
            _context.SaveChanges();
        }
    }
}