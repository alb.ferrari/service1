using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using service1.Models;
using Microsoft.EntityFrameworkCore;

namespace service1
{
    public class LogReader
    {
        private readonly ILogger<LogReader> _logger;
        private readonly Service1Context _context;

        public LogReader(ILogger<LogReader> logger, Service1Context context)
        {
            _logger = logger;
            _context = context;
        }

        public IEnumerable<LogItem> GetByPatternLINQ(string pattern)
        {
            return _context.LogItems.Where(item => item.Message.Contains(pattern)).AsEnumerable();
        }

        public IEnumerable<LogItem> GetByPatternSQL(string pattern)
        {
            var result = _context.LogItems.FromSqlRaw("SELECT * FROM service1.log_items WHERE message like {0}", "%" + pattern + "%").AsEnumerable();
            return result;
        }
        public IEnumerable<LogItem> GetAll()
        {
            return _context.LogItems.AsEnumerable();
        }
        public LogItem GetById(long id)
        {
            return _context.LogItems.Find(id);
        }
    }
}