# service1

This is a sample project that use .net core and postfres and implments a simple controller and a simple database layer using entity framework


Create a basic service that includes
	- vscode
	- Some rest api
	- Connection to a database
	- Health endpoint

From the prompt:
```
dotnet new webapi -o service1
cd service1
dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add package NpgSql.EntityFrameworkCore.PostgreSQL.Design
dotnet add package prometheus-net.AspNetCore
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Relational
dotnet add package EFCore.NamingConventions
dotnet tool install --global dotnet-ef
```

You can open the coject with vscode,
Assuming you have the c# extension already installed you get prompted for create the infrastructure say "yes"

To enable Prometheus endpoint in the Configure function in Startup.cs

Add dependency at the top:
```
using Prometheus;
```

Add Prometheus initialization code
```	
app.UseMetricServer();
app.UseHttpMetrics();
```
If you start the service with the debugger you have a working application

At this point you have a working application that export the api

GET https://localhost:5001/WeatherForecast

GET https://localhost:5001/metrics

Note this application have dependency to postgres framework but it does not connect to the database yet, we will  install postgres create a database, a user a schema for this application 

Install Postgres

```
sudo choco install postgresql --params '/Password:hammer'
sudo choco install pgadmin
```
Or install from here https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Create a database and a user (the postgres poassword is hammer or whatever you pass as parameter
```
psql -U postgres
> create database service1;
> create user user_service1;
> grant all privileges on database service1 to user_service1;
> alter user user_service1 with encrypted password 'hammer';
 psql -U user_service1 -d service1
> create schema service1;
```
To access the database with EF we need 2 classes under Models, one class represent the object that is going to be stored in the database the other class is the database context where we will define the database where the object class will be stored, the startup script will initialize the context

Appsetting.json will contain the connection string to the database

  "ConnectionStrings": {
    "DefaultConnection": "Host=localhost;Port=5432;Username=user_service1;Password=hammer;Database=service1;Search Path=service1,public"
  }

Note the option Search Path the first item is the schema where table are going to created.

Once the class are defined you need to create the structure in the database, this can be done with dotnet ef utility from the commanline

```
dotnet ef migrations add initial_step
dotnet ef database update
```

Any subsequent change to the database object require a new migration steps and an update of the schema, you can visulaize the creation scripts with all the steps using 

```
dotnet ef migrations script
```

Note that the steps are created as C# code under the Migrations directory, Migration steps can be execute automatically when the service starts.

Note that by default .net try to use camelcase while postgres is case insentive, this force .net to wrap every table and column identifier in double quotes, this is espeially annoying when you access the native aql level directly. To avoid this the library  EFCore.NamingConventions provide a qay to automatically convert camelcase in snakecase a convention much closer to the sql stanbdard 


Redis:
There are a couple of reccomended library, this one seems to be the right one https://stackexchange.github.io/StackExchange.Redis/
A lot of feature but is not too big and/or too abstract

TODO:
	- Can I use immutable object with EF ?
	- Connection to Redis for caching and queing
	- Unit testing


Reference:
https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code
https://medium.com/@agavatar/webapi-with-net-core-and-postgres-in-visual-studio-code-8b3587d12823
https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-overview
