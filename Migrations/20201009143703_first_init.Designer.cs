﻿// <auto-generated />
using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using service1.Models;

namespace service1.Migrations
{
    [DbContext(typeof(Service1Context))]
    [Migration("20201009143703_first_init")]
    partial class first_init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("service1.Models.LogItem", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Json")
                        .HasColumnName("json")
                        .HasColumnType("jsonb");

                    b.Property<string>("Message")
                        .HasColumnName("message")
                        .HasColumnType("text");

                    b.Property<JsonDocument>("Metadata")
                        .HasColumnName("metadata")
                        .HasColumnType("jsonb");

                    b.Property<long>("Priotity")
                        .HasColumnName("priotity")
                        .HasColumnType("bigint");

                    b.Property<Dictionary<string, string>>("Tags")
                        .HasColumnName("tags")
                        .HasColumnType("hstore");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnName("timestamp")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id")
                        .HasName("pk_log_items");

                    b.ToTable("log_items");
                });
#pragma warning restore 612, 618
        }
    }
}
